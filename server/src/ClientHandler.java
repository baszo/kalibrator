import java.net.DatagramPacket;

/**
 * Created by Andrzej on 2016-02-12.
 * Klasa dla każdego żadania klienckiego tworzy nowy wątek i go obłsuguje
 */
public class ClientHandler implements Runnable{
    Thread thread;
    DatagramPacket packet;
    ICalibrationServer parent;

    public ClientHandler(DatagramPacket packet, ICalibrationServer parent) {
        this.packet = packet;
        this.parent = parent;
        thread = new Thread(this);
        thread.start();
    }

    //wykonywanie operacji przysłąnej przez klienta
    public void run() {
            try {
                parent.executeCommand(packet, new String(packet.getData(), 0, packet.getLength()));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
    }

}
