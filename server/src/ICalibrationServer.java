import java.net.DatagramPacket;
import java.net.Socket;

/**
 * Created by Andrzej on 2016-02-12.
 * Interfejs służący ClientHandlerom na to jakie mają możliwośći korzystania z serwera
 */
public interface ICalibrationServer {
    void executeCommand(DatagramPacket packet, String command);
}
