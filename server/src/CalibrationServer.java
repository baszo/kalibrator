
import commands.Command;
import commands.CommandBody;
import commands.CommandsCoder;
import model.IDatabaseConnector;
import model.SqliteConnector;
import usedObjects.Configuration;
import usedObjects.DefaultSetting;

import javax.swing.*;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrzej on 2016-02-12.
 * Klasa reprezentująca serwer
 */
public class CalibrationServer implements ICalibrationServer, DefaultSetting {
    DatagramSocket serverSocket;
    IDatabaseConnector databaseConnector;
    List<Command> commandList;
    CommandsCoder commandsCoder;
    static int commandsExecuted = 0;
    static boolean stopExecution = false;
    private DatagramPacket Packet;
    private byte[] Data;

    public CalibrationServer() {
        try {
            int port = Integer.parseInt(Configuration.getInstance().getPropertyWithDefault("port",Integer.toString(DefaultSetting.defaultPort)));
            serverSocket = new DatagramSocket(port);
            databaseConnector = new SqliteConnector();
            commandsCoder= new CommandsCoder();
            generateCommands();
            JOptionPane.showMessageDialog(null, "Serwer został uruchomiony ", "Info",
                    JOptionPane.INFORMATION_MESSAGE);
            start();
        } catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Serwer już jest uruchomiony ", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /*****	Function To Send a Message to Client	**********/
    private void SendMessageToClient(DatagramPacket clientPacket,String message)
    {
        try {
            serverSocket.send(new DatagramPacket(message.getBytes(), message.getBytes().length, clientPacket.getAddress(), clientPacket.getPort()));
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * generujemy obsługe komend które będą wykonywane przez serwer
     */
    private void generateCommands() {
        commandList = new ArrayList<>();

        //komenda logowania
        commandList.add(new Command("Login") {
            @Override
            public void execute(DatagramPacket clientPacket, String command) {
                CommandBody body = commandsCoder.decodeCommand(command);

                String login = body.getBody().get(0);
                String password = body.getBody().get(1);

                if(databaseConnector.LoginUser(login, password)) {
                    SendMessageToClient(clientPacket,commandsCoder.encodeCommand("OK",null));
                }
                else {
                    SendMessageToClient(clientPacket, commandsCoder.encodeCommand("NOPE",null));
                }
            }
        });

        //komenda rejestracji
        commandList.add(new Command("REGISTER") {
            @Override
            public void execute(DatagramPacket clientPacket, String command) {
                CommandBody body = commandsCoder.decodeCommand(command);

                String login = body.getBody().get(0);
                String password = body.getBody().get(1);

                if(databaseConnector.registerUser(login, password)) {
                    SendMessageToClient(clientPacket,commandsCoder.encodeCommand("OK",null));
                }
                else {
                    SendMessageToClient(clientPacket, commandsCoder.encodeCommand("NOPE",null));
                }
            }
        });

        //rządanie pobrania rodzajów monitora
        commandList.add(new Command("GETMONITORTYPES") {
            @Override
            public void execute(DatagramPacket clientPacket, String command) {

                List<String> monitorTypes = databaseConnector.GetMonitorsType();
                String response = commandsCoder.encodeCommand("MONITORTYPES",monitorTypes);
                SendMessageToClient(clientPacket,response);
            }
        });

        //rządanie pobrania konfiguracji
        commandList.add(new Command("GETCONFIGS") {
            @Override
            public void execute(DatagramPacket clientPacket, String command) {

                List<String> monitorTypes = databaseConnector.GetConfigurations();
                String response = commandsCoder.encodeCommand("CONFIGS",monitorTypes);
                SendMessageToClient(clientPacket,response);
            }
        });

        //rządanie  dodania monitorów
        commandList.add(new Command("ADDMONITORS") {
            @Override
            public void execute(DatagramPacket clientPacket, String command) {
                CommandBody body = commandsCoder.decodeCommandWithUser(command);
                String response;
                if(databaseConnector.reconfigureMonitors(body.getUser(),body.getBody()))
                    response = commandsCoder.encodeCommand("SUCCESS",null);
                else
                    response = commandsCoder.encodeCommand("FAILED",null);
                SendMessageToClient(clientPacket,response);
            }
        });

        commandList.add(new Command("GETMONITORS") {
            @Override
            public void execute(DatagramPacket clientPacket, String command) {
                CommandBody body = commandsCoder.decodeCommandWithUser(command);

                List<String> monitors= databaseConnector.getMonitors(body.getUser());
                String response = commandsCoder.encodeCommand("MONITORS",monitors);
                SendMessageToClient(clientPacket,response);
            }
        });

        //rządanie zastopowania serwera
        commandList.add(new Command("STOP") {
            @Override
            public void execute(DatagramPacket clientPacket, String command) {
                stopExecution = true;
                while(commandsExecuted > 1){System.out.println("czekam");}
                SendMessageToClient(clientPacket,"SUCCES");
                System.exit(0);
            }
        });

        commandList.add(new Command("ADDVERIFICAION") {
            @Override
            public void execute(DatagramPacket clientPacket, String command) {
                CommandBody body = commandsCoder.decodeCommandWithUser(command);
                databaseConnector.addVerificationData(body.getUser(),body.getBody());
            }
        });

    }

    private void start() {
        while(true)
        {
            try{
                this.Data = new byte[1024];
                this.Packet = new DatagramPacket(Data, Data.length);
                System.out.println("Waiting for client...");
                serverSocket.receive(Packet);
                System.out.println("Odebrałem.");
                /******* Create a Seperate Thread for that each client**************/
                if(!stopExecution)
                    new ClientHandler(Packet,this);
            }
            catch ( Exception e ) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void executeCommand(DatagramPacket packet, String command) {
        CommandBody body = commandsCoder.decodeCommand(command);
        for (Command com: commandList) {
            if(body.getCommand().equals(com.getName())) {
                commandsExecuted++;
                com.execute(packet, command);
                break;
            }
        }
        commandsExecuted--;

    }
}
