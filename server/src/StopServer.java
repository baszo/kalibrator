import Connection.TcpWebConnection;
import Connection.WebConnection;
import commands.CommandsCoder;
import usedObjects.Configuration;
import usedObjects.DefaultSetting;

import javax.swing.*;

/**
 * Created by Andrzej on 2016-02-19.
 * Klasa stopująca serwer
 */
public class StopServer implements DefaultSetting {
    public static void main(String args[]) throws Exception
    {
        WebConnection connection = new TcpWebConnection(Integer.parseInt(Configuration.getInstance().getPropertyWithDefault("port",Integer.toString(DefaultSetting.defaultPort))),"127.0.0.1");
        CommandsCoder coder = new CommandsCoder();
        connection.sendMessageToServer(coder.encodeCommand("STOP",null));
        System.out.println(connection.reciveMessageFromServer());
        JOptionPane.showMessageDialog(null, "Serwer został zatrzymany ", "Info",
                JOptionPane.ERROR_MESSAGE);
    }
}
