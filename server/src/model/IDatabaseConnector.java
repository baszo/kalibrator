package model;

import java.util.List;

/**
 * Created by Andrzej on 2016-02-12.
 * Interfejs służący do komunikacji między serwerem a bazą danych
 */
public interface IDatabaseConnector {
    boolean LoginUser(String login, String password);
    List<String> GetMonitorsType();
    List<String> GetConfigurations();
    boolean insertMonitors(String user,List<String> data);
    boolean reconfigureMonitors(String user,List<String> data);

    List<String> getMonitors(String user);

    void addVerificationData(String user, List<String> body);

    boolean registerUser(String login, String password);
}
