package model;

import commands.CommandsCoder;
import org.sqlite.SQLiteConfig;
import usedObjects.CalibConfiguration;
import usedObjects.Configuration;
import usedObjects.Monitor;
import usedObjects.MonitorTypes;

import java.io.File;
import java.sql.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by Andrzej on 2016-02-12.
 * Klasa obsługujaca komunikacje z bazą sqlitową
 */
public class SqliteConnector implements IDatabaseConnector {

    Connection c;
    CommandsCoder coder = new CommandsCoder();
    String delimiter = coder.innerDelimiter;
    String dataBasePath = Configuration.getInstance().getPropertyWithDefault("dbPath","database.db");

    //konstruktor jak nie ma bazy tworzy ją
    public SqliteConnector() {
        try {
            if (!new File(dataBasePath).exists()) {
                CreateDatabase();
                MakeConnection();
            }
            else
                MakeConnection();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

    }

    //komenda dla sqlita tworząca baze
    private void CreateDatabase() {
        Statement stmt = null;
        MakeConnection();
        try {
            stmt = c.createStatement();
            String create="CREATE TABLE USERS("+
            "        ID        INTEGER PRIMARY KEY AUTOINCREMENT,"+
            "        PASSWORD       TEXT     NOT NULL,"+
            "        NAME           TEXT    NOT NULL"+
            ");"
            +
            "CREATE TABLE MONITORTYPE("+
            "        ID INTEGER PRIMARY KEY AUTOINCREMENT,"+
            "        MODEL           TEXT    NOT NULL,"+
            "        CORRECTION_TYPE     TEXT    NOT NULL"+
            ");"
            +
            "CREATE TABLE MONITOR("+
            "        ID INTEGER PRIMARY KEY AUTOINCREMENT,"+
            "        ID_USER     INTEGER,"+
            "        ID_MONITORTYPE     INTEGER,"+
            "        ID_CONFIGTYPE     INTEGER,"+
            "        INNER_ID           INTEGER,"+
            "        DETAILS           TEXT,"+
            "        FOREIGN KEY(ID_USER) REFERENCES USERS(ID),"+
            "        FOREIGN KEY(ID_MONITORTYPE) REFERENCES MONITORTYPE(ID),"+
            "        FOREIGN KEY(ID_CONFIGTYPE) REFERENCES CONFIGURATIONS(ID)"+
            ");"
            +
            "CREATE TABLE CONFIGURATIONS("+
            "        ID INTEGER PRIMARY KEY AUTOINCREMENT,"+
            "        NAME                         TEXT    NOT NULL,"+
            "        BRIGHTNES                    INTEGER NOT NULL,"+
            "        WHITE_CORRALATE_DAYLIGHT     INTEGER NOT NULL,"+
            "        GAMMA                        INTEGER NOT NULL"+
            "        );"
            +
            "CREATE TABLE KALIBRATION("+
            "        ID INTEGER PRIMARY KEY AUTOINCREMENT,"+
            "        ID_MONITOR     INTEGER,"+
            "        BRIGHTNES     INTEGER,"+
            "        WHITE_CORRALATE_DAYLIGHT     INTEGER,"+
            "        GAMMA                        NUMERIC,"+
            "        DATE DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            "        FOREIGN KEY(ID_MONITOR) REFERENCES MONITOR(ID)"+
            "        );";
            stmt.executeUpdate(create);
            c.commit();
            stmt.close();
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }

    }

    //tworzymy połączenie które będzie współdzielone
    private void MakeConnection(){
        try
        {
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();
            config.enforceForeignKeys(true);
            c = DriverManager.getConnection("jdbc:sqlite:"+ dataBasePath,config.toProperties());
            c.setAutoCommit(false);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    //logujemy użytkownika
    @Override
    public boolean LoginUser(String login, String password)
    {

        Statement stmt = null;
        ResultSet rs = null;
        boolean result = false;

        try {

            stmt = c.createStatement();
            rs = stmt.executeQuery("SELECT * FROM Users where name='" + login + "' and password='" + password + "';");
            if(rs.next())
            {
                result = true;
            }
            else
            {
                System.out.println("uzytkownik nie istnieje  ");
            }

        } catch ( Exception e ) {
            e.printStackTrace();
        }   finally {
            try { rs.close(); } catch (Exception e) { /* ignored */ }
            try { stmt.close(); } catch (Exception e) { /* ignored */ }
        }
        return result;
    }

    //pobieramy typy monitorów
    public List<String> GetMonitorsType()
    {

        Statement stmt = null;
        ResultSet rs = null;
        List<String> monitorTypes = new ArrayList<>();

        try {

            stmt = c.createStatement();
            rs = stmt.executeQuery("SELECT * FROM MONITORTYPE;");
            while(rs.next())
            {
                monitorTypes.add(rs.getString("MODEL")+ delimiter +rs.getString("CORRECTION_TYPE"));
            }

        } catch ( Exception e ) {
            e.printStackTrace();
        }   finally {
            try { rs.close(); } catch (Exception e) { /* ignored */ }
            try { stmt.close(); } catch (Exception e) { /* ignored */ }
        }
        return monitorTypes;
    }

    //pobieramy dosteny konfiguracje
    public List<String> GetConfigurations()
    {

        Statement stmt = null;
        ResultSet rs = null;
        List<String> configTypes = new ArrayList<>();

        try {

            stmt = c.createStatement();
            rs = stmt.executeQuery("SELECT * FROM CONFIGURATIONS;");
            while(rs.next())
            {
                configTypes.add(rs.getString("NAME")+ delimiter +rs.getInt("BRIGHTNES")+ delimiter +rs.getString("WHITE_CORRALATE_DAYLIGHT")+ delimiter +rs.getDouble("GAMMA"));
            }

        } catch ( Exception e ) {
            e.printStackTrace();
        }   finally {
            try { rs.close(); } catch (Exception e) { /* ignored */ }
            try { stmt.close(); } catch (Exception e) { /* ignored */ }
        }
        return configTypes;
    }

    //wsadzamy do bazy  nowemonitory
    @Override
    public boolean insertMonitors(String user, List<String> data) {
        ResultSet rs = null;
        boolean result = false;
        List<String[]> test = new ArrayList<String[]>();
        for(String record : data)
        {
            test.add(record.split(Pattern.quote(delimiter)));
        }

        try {

            for(String[] record : test) {
                int userId = c.createStatement().executeQuery("SELECT ID FROM USERS WHERE NAME = '" + user + "';").getInt("ID");
                int monitorId = c.createStatement().executeQuery("SELECT ID FROM MONITORTYPE WHERE MODEL = '" + record[1] + "';").getInt("ID");
                int configId = c.createStatement().executeQuery("SELECT ID FROM CONFIGURATIONS WHERE NAME = '" + record[0] + "';").getInt("ID");
                c.createStatement().executeUpdate("insert into MONITOR(ID,ID_USER,ID_MONITORTYPE,ID_CONFIGTYPE,INNER_ID,DETAILS) VALUES (NULL, '" + userId + "','" + monitorId + "', '" + configId + "','"  + record[2] + "',\"" + record[3] + "\" );");
                result = true;
            }

        } catch ( Exception e ) {
            e.printStackTrace();
        }   finally {
            try { rs.close(); } catch (Exception e) { /* ignored */ }
            try { c.commit(); } catch (Exception e) { /* ignored */ }

        }
        return result;
    }

    //sprawdzamy czy nowoskonfigurowane monitory trzeba zmienić o nich dane
    public boolean reconfigureMonitors(String user, List<String> data)
    {
        Statement stmt = null;
        ResultSet rs = null;
        Map<Integer,Monitor> oldMonitorList = new HashMap<>();

        List<String[]> test = new ArrayList<>();
        for(String record : data)
        {
            test.add(record.split(Pattern.quote(delimiter)));
        }

        try {

            stmt = c.createStatement();
            rs = stmt.executeQuery("select MONITOR.ID as MONITORID,NAME,BRIGHTNES,WHITE_CORRALATE_DAYLIGHT,GAMMA,MODEL,INNER_ID,DETAILS from CONFIGURATIONS,MONITORTYPE,MONITOR  where MONITOR.ID_CONFIGTYPE=CONFIGURATIONS.ID AND MONITORTYPE.ID=MONITOR.ID_MONITORTYPE AND MONITOR.ID_USER = (select ID from USERS where NAME = '"+user+"')\n");
            while(rs.next())
            {
                oldMonitorList.put(rs.getInt("MONITORID"),new Monitor(rs.getInt("INNER_ID"),rs.getString("DETAILS"),new CalibConfiguration(rs.getString("NAME"),rs.getInt("BRIGHTNES"),rs.getInt("WHITE_CORRALATE_DAYLIGHT"),rs.getDouble("GAMMA")), new MonitorTypes(rs.getString("MODEL"))));
            }
            Iterator<Map.Entry<Integer, Monitor>> iter = oldMonitorList.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<Integer,Monitor> entry = iter.next();
                Monitor monitor = entry.getValue();
                String[] tmp = {monitor.getConfig().getName(), monitor.getMonitorType().getModel(),Integer.toString(monitor.getId()),monitor.getDescription()};
                for(String[] www : test)
                    if(Arrays.equals(www,tmp)) {
                        iter.remove();
                        test.remove(www);
                        break;
                    }
            }

            //usuwanie starych danych
            iter = oldMonitorList.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<Integer,Monitor> entry = iter.next();
                stmt.executeUpdate("delete from KALIBRATION where \n" +
                        "(\n" +
                        "ID_MONITOR ='" +  entry.getKey() + "'\n" +
                        ")");
                stmt.executeUpdate("delete from MONITOR where \n" +
                        "(\n" +
                        "ID ='" +  entry.getKey() + "'\n" +
                        ")");
            }
            c.commit();

            //wstawienie nowych danych
            List<String> toAddData = new ArrayList<>();
            for(String[] each : test) {
                String tmp = String.join(delimiter,each);
                toAddData.add(tmp);
            }
            if(!toAddData.isEmpty())
                insertMonitors(user,toAddData);


        } catch ( Exception e ) {
            e.printStackTrace();
        }   finally {
            try { c.commit(); } catch (Exception e) { /* ignored */ }
            try { rs.close(); } catch (Exception e) { /* ignored */ }
            try { stmt.close(); } catch (Exception e) { /* ignored */ }
        }

        return true;

    }

    //pobieranie monitorów skonfigurowanych dla danego użytkonika
    @Override
    public List<String> getMonitors(String user) {
        Statement stmt = null;
        ResultSet rs = null;
        List<String> monitorList = new ArrayList<>();

        try {

            stmt = c.createStatement();
            rs = stmt.executeQuery("select * from CONFIGURATIONS,MONITORTYPE,MONITOR where MONITOR.ID_CONFIGTYPE=CONFIGURATIONS.ID AND MONITORTYPE.ID=MONITOR.ID_MONITORTYPE AND MONITOR.ID_USER = (select ID from USERS where NAME = '"+user+"')\n");
            while(rs.next())
            {
                monitorList.add(rs.getString("NAME")+ delimiter +rs.getInt("BRIGHTNES")+ delimiter +rs.getString("WHITE_CORRALATE_DAYLIGHT") + delimiter + rs.getDouble("GAMMA")+ delimiter +rs.getString("MODEL")+ delimiter +rs.getString("CORRECTION_TYPE")+ delimiter +rs.getString("INNER_ID")+ delimiter +rs.getString("DETAILS"));
            }

        } catch ( Exception e ) {
            e.printStackTrace();
        }   finally {
            try { rs.close(); } catch (Exception e) { /* ignored */ }
            try { stmt.close(); } catch (Exception e) { /* ignored */ }
        }
        return monitorList;
    }

    //dodawanie danych z weryfikacji monitora
    @Override
    public void addVerificationData(String user, List<String> data) {
        ResultSet rs = null;
        List<String[]> test = new ArrayList<>();
        for(String record : data)
        {
            test.add(record.split(Pattern.quote(delimiter)));
        }

        try {

            for(String[] record : test) {
                int userId = c.createStatement().executeQuery("SELECT ID FROM USERS WHERE NAME = '" + user + "';").getInt("ID");
                int monitorId = c.createStatement().executeQuery("SELECT ID FROM MONITOR WHERE INNER_ID = '" + record[0] + "' AND ID_USER = '" + userId + "';").getInt("ID");
                c.createStatement().executeUpdate("insert into KALIBRATION(ID,ID_MONITOR,BRIGHTNES,WHITE_CORRALATE_DAYLIGHT,GAMMA) VALUES (NULL, '" + monitorId + "','" + record[1] + "', '" + record[3] + "','"  + record[2] + "' );");
            }

        } catch ( Exception e ) {
            e.printStackTrace();
        }   finally {
            try { rs.close(); } catch (Exception e) { /* ignored */ }
            try { c.commit(); } catch (Exception e) { /* ignored */ }

        }

    }

    @Override
    public boolean registerUser(String login, String password) {
        {

            Statement stmt = null;
            ResultSet rs = null;
            boolean result = false;

            try {

                stmt = c.createStatement();
                rs = stmt.executeQuery("SELECT * FROM Users where name='" + login + "';");
                if(rs.next())
                {
                    result = false;
                }
                else
                {
                    c.createStatement().executeUpdate("insert into USERS(ID,PASSWORD,NAME) VALUES (NULL, '" + password + "','" + login + "' );");
                    result = true;
                }

            } catch ( Exception e ) {
                e.printStackTrace();
            }   finally {
                try { rs.close(); } catch (Exception e) { /* ignored */ }
                try { c.commit(); } catch (Exception e) { /* ignored */ }
                try { stmt.close(); } catch (Exception e) { /* ignored */ }
            }
            return result;
        }
    }
}
