package View;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class IdentifyFrame extends JFrame implements ActionListener {

    // constructor
    IdentifyFrame() {
        setUndecorated(true);
        Timer t = new Timer(1000, this);    // Timer in 10 seconds
        t.start();
    }

    // will be called after 10000 milliseconds
    public void actionPerformed(ActionEvent e) {
        this.dispose();
    }
}