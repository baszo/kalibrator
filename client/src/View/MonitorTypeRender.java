package View;


import usedObjects.MonitorTypes;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import java.awt.*;

/**
 * Created by Andrzej on 2016-02-17.
 * Klasa do wyświetlania rodzaju monitorów pobranych z serwera w liście rozwijalnej
 */
public class MonitorTypeRender extends BasicComboBoxRenderer {
    public Component getListCellRendererComponent(
            JList list, Object value, int index,
            boolean isSelected, boolean cellHasFocus)
    {
        super.getListCellRendererComponent(list, value, index,
                isSelected, cellHasFocus);

        if (value != null)
        {
            MonitorTypes item = (MonitorTypes)value;
            setText(item.getModel().toUpperCase() );
        }

        if (index == -1)
        {
            MonitorTypes item = (MonitorTypes)value;
            setText(item.getModel().toUpperCase() );
        }


        return this;
    }
}
