package View;


import usedObjects.Monitor;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import java.awt.*;

/**
 * Created by Andrzej on 2016-02-17.
 * Klasa do wyświetlania rodzaj monitora pobranej z serwera w liście rozwijalnej
 */
public class MonitorDetailRender extends BasicComboBoxRenderer {
    public Component getListCellRendererComponent(
            JList list, Object value, int index,
            boolean isSelected, boolean cellHasFocus)
    {
        super.getListCellRendererComponent(list, value, index,
                isSelected, cellHasFocus);

        if (value != null)
        {
            Monitor item = (Monitor)value;
            setText(Integer.toString(item.getId()) );
        }

        if (index == -1)
        {
            Monitor item = (Monitor)value;
            setText(Integer.toString(item.getId()) );
        }


        return this;
    }
}
