package View;

import Connection.TcpWebConnection;
import Connection.WebConnection;
import Controller.IMainCalibrationController;
import Controller.MainCalibraionController;
import usedObjects.DefaultSetting;
import usedObjects.Monitor;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Andrzej on 2016-02-17.
 * Główne okno do kalibracji
 */
public class MainCalibrationPage  extends JFrame implements IWindows, DefaultSetting {
    private JPanel mainPanel;
    private JLabel formName;
    private JPanel configData;
    private JButton editButton;
    private IMainCalibrationController controller;

    //główny konstruktor
    public MainCalibrationPage(WebConnection connection, boolean visible) {

        super("Kalibrator");
        controller = new MainCalibraionController(connection,this);
        configData.setLayout(new BoxLayout(configData, BoxLayout.Y_AXIS));
        setContentPane(mainPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        genMonitorsContent();
        pack();
        setLocationRelativeTo(null);
        setVisible(visible);
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                new ConfigWindows(connection,controller.getMonitors());
            }
        });
    }

    //konstruktor wywoływany podczas systemowego harmonogramu w celach uruchomienia weryfikacji
    public MainCalibrationPage(int monitorId, String description, String user,String serverAddres) {
        this(new TcpWebConnection(defaultPort,serverAddres,user),false);
        Object[] options = {"Wykonaj Weryfikacjie",
                "Odłóż na później"};
        int n = JOptionPane.showOptionDialog(this,
                "Czy chcesz wykonać weryfikacje kalibracji monitora ?",
                "Kalibrator[Weryfikacja cykliczna]",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[0]);
    //wykonać weryfikacje czy odłozyc na jutro
    if(n==0) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        int id  = 1;
        JFrame frame = genFrameToIdentify();
        for (GraphicsDevice gd : ge.getScreenDevices()) {
            if(id == monitorId) {
                frame.setUndecorated(true);
                frame.setLocation(getMiddleX(gd.getDefaultConfiguration().getBounds(), frame.getWidth()), getMiddleY(gd.getDefaultConfiguration().getBounds(), frame.getHeight()));
                frame.setVisible(true);
                break;
            }
            id++;
        }
        JFrame dialog = new JFrame();
        dialog.setSize(100, 100);
        dialog.setLocation(frame.getX(),frame.getY());
        dialog.setVisible(true);
        JOptionPane.showMessageDialog(dialog, "Kontynuja", "",
                JOptionPane.INFORMATION_MESSAGE);
        dialog.dispose();
        frame.dispose();
        if(!controller.verification(monitorId, description))
            setVisible(true);
        else
            System.exit(0);
    }
    else {
        controller.addSchedulerVerificationOneTime(monitorId, description);
        System.exit(0);
    }
}

    //pobieramy monitory i generujemy okno na podstawie pobranych danych
    private void genMonitorsContent() {
        for(Monitor mon : controller.getMonitors())
        {
            JLabel name = new JLabel(mon.getId()+"."+mon.getMonitorType().getModel());
            name.setFont(new Font(name.getName(), Font.PLAIN, 20));
            name.setAlignmentX(Component.CENTER_ALIGNMENT);
            JButton calibrate = new JButton("Szybka kalibracja");
            calibrate.setLayout(new FlowLayout());
            calibrate.setAlignmentX(Component.CENTER_ALIGNMENT);
            JButton advanceCalibrate = new JButton("Zaawansowana kalibracja");
            advanceCalibrate.setLayout(new FlowLayout());
            advanceCalibrate.setAlignmentX(Component.CENTER_ALIGNMENT);
            calibrate.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
                    int id  = 1;
                    JFrame frame = genFrameToIdentify();
                    for (GraphicsDevice gd : ge.getScreenDevices()) {
                        if(id == mon.getId()) {
                            frame.setUndecorated(true);
                            frame.setLocation(getMiddleX(gd.getDefaultConfiguration().getBounds(), frame.getWidth()), getMiddleY(gd.getDefaultConfiguration().getBounds(), frame.getHeight()));
                            frame.setVisible(true);
                            break;
                        }
                        id++;
                    }
                    JFrame dialog = new JFrame();
                    dialog.setSize(100, 100);
                    dialog.setLocation(frame.getX(),frame.getY());
                    dialog.setVisible(true);
                    JOptionPane.showMessageDialog(dialog, "Kontynuja", "",
                            JOptionPane.INFORMATION_MESSAGE);
                    dialog.dispose();
                    frame.dispose();
                    setVisible(false);
                    controller.executeBasicCalibrtion(mon);
                    setVisible(true);
                }
            });
            advanceCalibrate.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    controller.executeAdvanceCalibration(mon);
                    setVisible(true);
                }
            });
            Border loweredbevel = BorderFactory.createLoweredBevelBorder();
            Border raisedbevel = BorderFactory.createRaisedBevelBorder();
            JPanel monitorPanel = new JPanel();
            monitorPanel.setBorder(BorderFactory.createCompoundBorder(raisedbevel, loweredbevel));
            monitorPanel.add(name);
            monitorPanel.add(calibrate);
            monitorPanel.add(advanceCalibrate);
            configData.add(monitorPanel);
        }
    }

    //dla kontrolera aby mógł pokazywać błędy podczas jego przetwarzania
    @Override
    public void showError(String message) {
        JOptionPane.showMessageDialog(new Frame(), message, "Błąd",
                JOptionPane.ERROR_MESSAGE);
    }

    //wygeneruj okno do identyfikacji ekranu
    private JFrame genFrameToIdentify()
    {
        JFrame frame = new JFrame();
        frame.setLayout(new GridBagLayout());
        JPanel panel = new JPanel();
        JLabel jlabel = new JLabel("Umiesc tutaj urządzenie");
        jlabel.setFont(new Font("Verdana",1,20));
        panel.add(jlabel);
        panel.setBorder(new LineBorder(Color.BLACK)); // make it easy to see
        frame.add(panel, new GridBagConstraints());
        frame.setSize(400, 400);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        return frame;
    }
    //punkt w osi x aby ustawić okno na środku
    static int getMiddleX(Rectangle bounds, int frameWidth){
        return bounds.x + bounds.width/2 - frameWidth/2;
    }

    //punkt w osi y aby ustawić okno na środku
    static int getMiddleY(Rectangle bounds, int frameHigh){
        return bounds.y + bounds.height/2 - frameHigh/2;
    }




}
