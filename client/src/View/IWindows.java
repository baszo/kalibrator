package View;

/**
 * Created by Andrzej on 2016-02-18.
 * interfejs dla controlerów aby mogli informować usera o błędach
 */
public interface IWindows {
    void showError(String message);
}
