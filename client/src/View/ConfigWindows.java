package View;

import Connection.WebConnection;
import Controller.ConfigController;
import Controller.IConfigController;
import usedObjects.CalibConfiguration;
import usedObjects.Monitor;
import usedObjects.MonitorTypes;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.*;
import java.util.List;

/**
 * Created by Andrzej on 2016-02-12.
 */
public class ConfigWindows extends JFrame implements IWindows {
    private JPanel mainPanel;
    private JButton addMonitorButton;
    private JButton approveButton;
    private JPanel downPanel;
    private JPanel monitorPanel;
    private JButton cancelButton;
    private JButton ientify;
    private IConfigController controller;
    public Map<Integer,JComboBox<MonitorTypes>> monitorsTypeMap;
    public Map<Integer,JComboBox<CalibConfiguration>> calibrationConfigMap;
    public Map<Integer,JComboBox<Monitor>> monitorDetailsMap;
    int numberOfMoniotrs = 1;
    int sumOfMoniors=0;


    public ConfigWindows(WebConnection connection, List<Monitor> monitors)
    {
        this();
        controller = new ConfigController(connection,this);
        addMonitorComponents(monitors);
        cancelButton.setVisible(true);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public ConfigWindows(WebConnection connection) {
        this();
        controller = new ConfigController(connection,this);
        cancelButton.setVisible(false);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }


    private ConfigWindows(){
        super("Kalibrator");
        monitorsTypeMap = new HashMap<>();
        calibrationConfigMap = new HashMap<>();
        monitorDetailsMap = new HashMap<>();
        monitorPanel.setLayout(new BoxLayout(monitorPanel, BoxLayout.Y_AXIS));
        approveButton.setLayout(new BorderLayout());
        setContentPane(mainPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addMonitorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addMonitorComponent(null);
            }
        });
        approveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Monitor> monitors = new ArrayList<>();
                if(checkVAlidation()) {
                    for (Map.Entry<Integer, JComboBox<MonitorTypes>> entry : monitorsTypeMap.entrySet()) {
                        Integer key = entry.getKey();
                        MonitorTypes monitorValue = (MonitorTypes) entry.getValue().getSelectedItem();
                        CalibConfiguration calibConfigValue = (CalibConfiguration) calibrationConfigMap.get(key).getSelectedItem();
                        Monitor monitor = (Monitor) monitorDetailsMap.get(key).getSelectedItem();
                        monitor.setMonitorType(monitorValue);
                        monitor.setConfig(calibConfigValue);
                        monitors.add(monitor);
                    }
                    if (!controller.configureMonitors(monitors)) {
                        JOptionPane.showMessageDialog(new Frame(), "Problemy z serwerem!", "Błąd",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        dispose();
                        new MainCalibrationPage(controller.getConnection(),true);
                    }
                }
                else
                {
                    showError("Dwa lub więcej monitorów są przypisane do tego samego identyfikatora!!");
                }

            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                new MainCalibrationPage(controller.getConnection(),true);
            }
        });
        ientify.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
                int numberOfScreen = 1;
                for (GraphicsDevice gd : ge.getScreenDevices()) {
                    JFrame frame = genFrameToIdentify(numberOfScreen);
                    frame.setLocation(getMiddleX(gd.getDefaultConfiguration().getBounds(),frame.getWidth()), getMiddleY(gd.getDefaultConfiguration().getBounds(),frame.getHeight()));
                    frame.setVisible(true);
                    numberOfScreen++;
                }
            }
        });
    }

    private boolean checkVAlidation() {
        final Set<Integer> setToReturn = new HashSet();
        final Set<Integer> set1 = new HashSet();

        for (JComboBox<Monitor> box : monitorDetailsMap.values())
        {
            if (!set1.add(((Monitor)box.getSelectedItem()).getId()))
            {
                setToReturn.add((((Monitor) box.getSelectedItem()).getId()));
            }
        }

        return set1.size()==monitorDetailsMap.size();
    }

    //punkt w osi x aby ustawić okno na środku
    static int getMiddleX(Rectangle bounds, int frameWidth){
        return bounds.x + bounds.width/2 - frameWidth/2;
    }

    //punkt w osi y aby ustawić okno na środku
    static int getMiddleY(Rectangle bounds, int frameHigh){
        return bounds.y + bounds.height/2 - frameHigh/2;
    }

    //wygeneruj okno do identyfikacji ekranu
    private JFrame genFrameToIdentify(int number)
    {
        JFrame frame = new IdentifyFrame();
        frame.setLayout(new GridBagLayout());
        JPanel panel = new JPanel();
        JLabel jlabel = new JLabel(Integer.toString(number));
        jlabel.setFont(new Font("Verdana",1,150));
        panel.add(jlabel);
        panel.setBorder(new LineBorder(Color.BLACK)); // make it easy to see
        frame.add(panel, new GridBagConstraints());
        frame.setSize(400, 400);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        return frame;
    }

    //dodaj komponenty potrzebne do dodania kolejnego ekarnu
    private void addMonitorComponent(Monitor monitor) {
        JComboBox calibConfigBox = new JComboBox();
        JComboBox monitorTypeBox = new JComboBox();
        JComboBox monitorDetailBox = new JComboBox();
        JButton monitorButton = new JButton("Usuń monitor ");
        monitorButton.setLayout(new FlowLayout());
        int index = numberOfMoniotrs;
        List<Monitor> monitorLocalDetail = controller.getMonitorsDetails();
        if(sumOfMoniors >= monitorLocalDetail.size()) {
            JOptionPane.showMessageDialog(new Frame(), "Nie można dodać więcej monitorów", "Informacja",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        for (Monitor moonitorDetail:monitorLocalDetail)
        {
            monitorDetailBox.addItem(moonitorDetail);
            if(monitor!=null && moonitorDetail.getDescription().equals( monitor.getDescription()))
                monitorDetailBox.setSelectedItem(moonitorDetail);
        }

        for(CalibConfiguration config : controller.getCalibConfiguration())
        {
            calibConfigBox.addItem(config);
                if(monitor!=null && config.getName().equals( monitor.getConfig().getName()))
                    calibConfigBox.setSelectedItem(config);
        }
        for(MonitorTypes monitorType : controller.getMonitorsConfigurations())
        {
            monitorTypeBox.addItem(monitorType);
            if(monitor!=null && monitorType.getModel().equals(monitor.getMonitorType().getModel()))
                monitorTypeBox.setSelectedItem(monitorType);
        }
        monitorButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                monitorPanel.remove(monitorTypeBox);
                monitorPanel.remove(calibConfigBox);
                monitorPanel.remove(monitorButton);
                monitorPanel.remove(monitorDetailBox);
                monitorsTypeMap.remove(index);
                calibrationConfigMap.remove(index);
                monitorDetailsMap.remove(index);
                pack();
                repaint();
                sumOfMoniors--;
            }
        });
        calibConfigBox.setRenderer( new CalibConfigurationRender() );
        monitorTypeBox.setRenderer(new MonitorTypeRender());
        monitorDetailBox.setRenderer(new MonitorDetailRender());
        monitorsTypeMap.put(numberOfMoniotrs,monitorTypeBox);
        calibrationConfigMap.put(numberOfMoniotrs,calibConfigBox);
        monitorDetailsMap.put(numberOfMoniotrs,monitorDetailBox);
        monitorPanel.add(monitorTypeBox);
        monitorPanel.add(calibConfigBox);
        monitorPanel.add(monitorDetailBox);
        monitorPanel.add(monitorButton);
        pack();
        repaint();
        numberOfMoniotrs++;
        sumOfMoniors++;
    }

    //dodaj już skonfigurowane monitory do edycji
    private void addMonitorComponents(List<Monitor> monitors) {
        for(Monitor monitor : monitors) {
            addMonitorComponent(monitor);
        }
    }


    //wyświetl problemy z controllera
    @Override
    public void showError(String message) {
        JOptionPane.showMessageDialog(new Frame(), message, "Błąd",
                JOptionPane.ERROR_MESSAGE);
    }

    class ItemChangeListener implements ItemListener {
        @Override
        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                Monitor item = (Monitor) event.getItem();
                for (Map.Entry<Integer, JComboBox<Monitor>> entry : monitorDetailsMap.entrySet())
                {
                    if(item.getId() == ((Monitor)entry.getValue().getSelectedItem()).getId()) {
                        System.out.println("asdas");
                    }
                }
                // do something with object
            }
        }
    }
}
