package View;

import usedObjects.CalibConfiguration;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import java.awt.*;

/**
 * Created by Andrzej on 2016-02-17.
 * Klasa do wyświetlania konfiguracji pobranej z serwera w liście rozwijalnej
 */
public class CalibConfigurationRender extends BasicComboBoxRenderer {
    public Component getListCellRendererComponent(
            JList list, Object value, int index,
            boolean isSelected, boolean cellHasFocus)
    {
        super.getListCellRendererComponent(list, value, index,
                isSelected, cellHasFocus);

        if (value != null)
        {
            CalibConfiguration item = (CalibConfiguration)value;
            setText( item.getName().toUpperCase() );
        }

        if (index == -1)
        {
            CalibConfiguration item = (CalibConfiguration)value;
            setText( "" + item.getName() );
        }


        return this;
    }
}
