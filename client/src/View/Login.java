package View;

import Controller.ILoginController;
import Controller.LoginController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Andrzej on 2016-02-12.
 * Klasa reprezentująca okno logowania
 */
public class Login  extends JFrame implements IWindows{
    private JButton loginButton;
    private JPanel mainPanel;
    private JTextField loginText;
    private JLabel loginLabe;
    private JLabel passwordLabel;
    private JPasswordField passwordText;
    private JCheckBox remamberCheckBox;
    private JLabel serverAddresLabel;
    private JFormattedTextField serverAddresText;
    private JButton registerButton;

    private ILoginController controller;


    /**
     * Sprawdzanie poprawności wypełnienia pól
     *
     * @return czy poprawne
     */
    private boolean validateFields(){
        boolean valid =!passwordText.getText().isEmpty() && !loginText.getText().isEmpty() && !serverAddresText.getText().isEmpty();
        if(!valid)
            JOptionPane.showMessageDialog(new Frame(), "Pola wypełnione niepoprawnie!", "Błąd",
                    JOptionPane.ERROR_MESSAGE);
        return valid;

    }


    /**
     * Konstruktor
     */
    public Login(){
        super("Login");
        controller = new LoginController(this);
        setContentPane( mainPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);


        serverAddresText.setInputVerifier(new InputVerifier() {
            //walidacja ip
            Pattern pat = Pattern.compile("\\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b");
            public boolean shouldYieldFocus(JComponent input) {
                boolean inputOK = verify(input);
                if (inputOK) {
                    return true;
                } else {
                    JOptionPane.showMessageDialog(new Frame(), "Zły format Ip!", "Błąd",
                            JOptionPane.ERROR_MESSAGE);
                    return false;
                }
            }
            public boolean verify(JComponent input) {
                JTextField field = (JTextField) input;
                Matcher m = pat.matcher(field.getText());
                return m.matches();
            }
        });
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(validateFields()) {
                    if(controller.login(loginText.getText(), passwordText.getText(), serverAddresText.getText(), remamberCheckBox.isSelected()))
                    {
                        dispose();
                        new MainCalibrationPage(controller.getConnection(),true);
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "Logowanie zakończyło się niepowodzeniem ", "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(controller.register(loginText.getText(), passwordText.getText(), serverAddresText.getText(), remamberCheckBox.isSelected()))
                {
                    dispose();
                    new ConfigWindows(controller.getConnection());
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "Rejestracja zakończyła się niepowodzeniem ", "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }


    @Override
    public void showError(String message) {
        JOptionPane.showMessageDialog(new Frame(), message, "Błąd",
                JOptionPane.ERROR_MESSAGE);
    }
}
