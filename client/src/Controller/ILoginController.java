package Controller;


import Connection.WebConnection;

/**
 * Created by Andrzej on 2016-02-12.
 */
public interface ILoginController {
    boolean login(String login, String password, String hostAddres, boolean rememberData);
    boolean register(String login, String password, String hostAddres, boolean rememberData);
    WebConnection getConnection();
}
