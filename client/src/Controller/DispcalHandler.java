package Controller;

import Connection.WebConnection;
import main.StartClient;
import usedObjects.Monitor;

import javax.swing.*;
import java.io.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * Created by Andrzej on 2016-02-19.
 * Klasa obsługująca operacje na programie dispcal
 */
public class DispcalHandler extends JFrame implements  IdispcalHandler{
    Thread thread,threadError;
    static String test = "";
    static String errorMesage = "";
    Object lock = new Object();
    Boolean error = false;
    Boolean run = true;
    List<String> verificationData = new ArrayList<>();


    //Funkcja zwracająca za pomocą programu dispcal informacje o podmontowanych monitorach
    public List<Monitor> getMoniotrsDetails()
    {
        List<Monitor> informations = new ArrayList<>();
        String info = executeInfoCommand("data\\dispcal");
        for(String line : info.split("\\r?\\n"))
        {
            //parsuje dane i wyciągam informacje o dostęnych monitorach
            if(line.matches("    [1-9]+ = 'D(.*)")) {
                String[] data = line.replaceAll("^\\s+","").split("=");
                informations.add(new Monitor(Integer.parseInt(data[0].replaceAll("\\s+$","")),data[1]));
            }
        }
        return informations;
    }

    //wykonuje polecenie 'dispcal' w systemie i pobiera informacje
    private String executeInfoCommand(String comand) {
        System.setProperty("file.encoding", "UTF-8");
        String all="";
        try{
            Process proc = Runtime.getRuntime().exec(comand);
            InputStream stdin = proc.getErrorStream();
            InputStreamReader isr = new InputStreamReader(stdin, "CP852");
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            while ( (line = br.readLine()) != null)

                all+=line + "\n";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return all;
    }

    //wykonanie podstawowej konfiruracji monitora
    public void executeCalibration(Monitor mon)
    {
        String profileName = mon.getMonitorType().getModel().split("[^\\w']+")[0]+Integer.toString(mon.getId());
        executeCommand("data\\dispcal -Y p  -d"+mon.getId()+" -qv -X data\\"+ mon.getMonitorType().getCorrectionType()+" -m -t"+mon.getConfig().getWhiteCorrolateDaylight()+" -b"+mon.getConfig().getBrightness()+" -g"+mon.getConfig().getGamma()+" -o data"+File.separator+profileName);
        File file = new File("data"+File.separator+profileName+ ".icm");
        if(file.exists())
            finishCalibration(profileName);
    }

    //wykonuje zaawansowaną klaiibracje podanego monitora
    public void executeAdvanceCalibration(Monitor mon) {
        try {
            String profileName = mon.getMonitorType().getModel().split("[^\\w']+")[0]+Integer.toString(mon.getId());
            Process proc = Runtime.getRuntime().exec("cmd.exe /c start /wait cmd.exe /c \"data\\dispcal -d1 -qv -X data\\"+ mon.getMonitorType().getCorrectionType()+" -m -t"+mon.getConfig().getWhiteCorrolateDaylight()+" -b"+mon.getConfig().getBrightness()+" -g"+mon.getConfig().getGamma()+" -o data"+File.separator+profileName);
            int exitVal = proc.waitFor();
            File file = new File("data"+File.separator+profileName+ ".icm");
            if(file.exists())
                finishCalibration(profileName);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //dodanie do schedulera jednorazowego wykonania
    public void addOneTimeSchedulerVeryfication(Monitor m, String User, WebConnection c) {
        try {
            List<String> commands = new ArrayList<>();
            File file = new File(StartClient.class.getProtectionDomain().getCodeSource().getLocation().toURI());
            String path = file.getAbsolutePath()+"\\"+file.getName()+".exe";
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            DateFormat hourFormat = new SimpleDateFormat("HH:mm");
            Calendar date = Calendar.getInstance();
            date.setTime(new Date());
            date.add(Calendar.DATE, 1);

            commands.add("schtasks.exe");
            commands.add("/CREATE");
            commands.add("/TN");
            commands.add("\"CalibOnce" + m.getId() + "\"");
            commands.add("/TR");
            commands.add("\" " + path + " " + m.getId() + " " + m.getDescription() + " " + User + " " + c.getServerAddres() + "\"");
            commands.add("/SC");
            commands.add("ONCE");
            commands.add("/ST");
            commands.add(hourFormat.format(new Date()));
            commands.add("/SD");
            commands.add(dateFormat.format(date.getTime()));

            ProcessBuilder builder = new ProcessBuilder(commands);
            Process p = null;
            p = builder.start();

            p.waitFor();
            System.out.println(p.exitValue());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //po wykonaniu kalibracji instalowany jest wygenerowany profil
    private void finishCalibration(String profile) {
        try {
            Process proc = Runtime.getRuntime().exec("data\\dispwin -d1 -I "+File.separator+profile+ ".icm");
            int exitVal = proc.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    //dodanie do schtask przyszłej weryfikacji
    public void addSchedulerVeryfication(Monitor m, String User, WebConnection c)
    {

        if(!SchedulerExist("Calib"+m.getId())) {
            try {
                List<String> commands = new ArrayList<>();
                File file = new File(StartClient.class.getProtectionDomain().getCodeSource().getLocation().toURI());
                String path = file.getAbsolutePath()+"\\"+file.getName()+".exe";
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                DateFormat hourFormat = new SimpleDateFormat("HH:mm");
                Date date = new Date();
                System.out.println(dateFormat.format(date));

                commands.add("schtasks.exe");
                commands.add("/CREATE");
                commands.add("/TN");
                commands.add("\"Calib" + m.getId() + "\"");
                commands.add("/TR");
                commands.add("\" " + path + " " + m.getId() + " " + m.getDescription() + " " + User + " " + c.getServerAddres() + "\"");
                commands.add("/SC");
                commands.add("WEEKLY");
                commands.add("/ST");
                commands.add(hourFormat.format(new Date()));
                commands.add("/SD");
                commands.add(dateFormat.format(date));

                ProcessBuilder builder = new ProcessBuilder(commands);
                Process p = null;
                p = builder.start();

                p.waitFor();
                System.out.println(p.exitValue());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //sprawdzanie czy dla danego monitora istnieje już harmonogram
    private boolean SchedulerExist(String name) {
        String commmand = "schtasks /query /tn \""+name+"\"";
        boolean exist = false;
        try {
            Process proc = Runtime.getRuntime().exec(commmand);
            InputStream stderr = proc.getInputStream();
            InputStreamReader isr = new InputStreamReader(stderr);
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            while ( (line = br.readLine()) != null)
            {
                exist = true;
            }
            proc.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return exist;

    }

    //usuwa harmonogram gdy kalibracja się zmieniła
    public void deleteScheduler(int monitorId)
    {
        String deleteCommand = "schtasks /delete /tn "+"Calib"+monitorId+" /f";
        executeCommand(deleteCommand);
    }

    //wykonanie podanej w prametrze funkcji systemowej
    public void executeCommand(String comand) {
        try{
            Process proc = Runtime.getRuntime().exec(comand);
            InputStreamReader isr = new InputStreamReader(proc.getInputStream(), "CP852");
            BufferedReader br = new BufferedReader(isr);
            startReadVerification(proc.getErrorStream());

            String line = null;
            while ( (line = br.readLine()) != null) {
                System.out.println(line);
                if (line.contains("Spot read failed due to the sensor being in the wrong position")) {
                    JFrame frmOpt = new JFrame();
                    frmOpt.setLocation(100, 100);
                    frmOpt.setAlwaysOnTop(true);
                    JOptionPane.showMessageDialog(frmOpt, line, "Błąd", JOptionPane.ERROR_MESSAGE);
                    error = true;
                    break;
                }
                if (line.contains("dispcal: Error - new_disprd()")) {
                    JFrame frmOpt = new JFrame();
                    frmOpt.setLocation(100, 100);
                    frmOpt.setAlwaysOnTop(true);
                    JOptionPane.showMessageDialog(frmOpt, line, "Błąd", JOptionPane.ERROR_MESSAGE);
                    errorMesage = line;
                    error = true;
                    break;
                }
                if (line.contains("dispcal: Error - No instrument")) {
                    JFrame frmOpt = new JFrame();
                    frmOpt.setLocation(100, 100);
                    frmOpt.setAlwaysOnTop(true);
                    JOptionPane.showMessageDialog(frmOpt, line, "Błąd", JOptionPane.ERROR_MESSAGE);
                    errorMesage = line;
                    error = true;
                    break;
                }
            }
            if(error)
                proc.destroy();
            else
                proc.waitFor();

            thread.stop();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //czytanie danych z weryfikacji i obsługa podstawowych rządań
    public void startReadVerification( InputStream stdout){
        Runnable writeTask = new Runnable() {
            @Override
            public void run() {
                try {
                    InputStreamReader isr = new InputStreamReader(stdout, "CP852");
                    BufferedReader br = new BufferedReader(isr);
                    String line = null;
                    while ( true) {
                        while ((line = br.readLine()) != null) {
                            System.out.println(line);
                            if(line.contains("Spot read failed due to the sensor being in the wrong position")) {
                                JFrame frmOpt = new JFrame();
                                frmOpt.setLocation(100, 100);
                                frmOpt.setAlwaysOnTop(true);
                                JOptionPane.showMessageDialog(frmOpt, "Umieść poprawnie urządzenie albo zdjemi osłone czujnika. Następnie w oknie poleceni wciśni dowolny przycisk aby kontynuować","Błąd",JOptionPane.ERROR_MESSAGE);
                                errorMesage = line;
                                error=true;
                            }
                            if(line.contains("dispcal: Error - new_disprd()"))
                            {
                                JFrame frmOpt = new JFrame();
                                frmOpt.setLocation(100, 100);
                                frmOpt.setAlwaysOnTop(true);
                                JOptionPane.showMessageDialog(frmOpt, line,"Błąd",JOptionPane.ERROR_MESSAGE);
                                errorMesage = line;
                                error=true;
                                synchronized (lock) {
                                    lock.notify();
                                }
                                return;
                            }
                            if(line.contains("dispcal: Error - No instrument"))
                            {
                                JFrame frmOpt = new JFrame();
                                frmOpt.setLocation(100, 100);
                                frmOpt.setAlwaysOnTop(true);
                                JOptionPane.showMessageDialog(frmOpt, line,"Błąd",JOptionPane.ERROR_MESSAGE);
                                errorMesage = line;
                                error=true;
                                synchronized (lock) {
                                    lock.notify();
                                }
                                return;
                            }
                            if(line.contains("Aprox. gamma"))
                            {
                                String[] data = line.split("=");
                                verificationData.add(data[1]);
                            }
                            if(line.contains("White Correlated Daylight"))
                            {
                                String[] data = line.split("=");
                                String white = data[1].split(",")[0];
                                verificationData.add(white.substring(0,white.length()-1));
                                synchronized (lock) {
                                    lock.notify();
                                }
                            }
                            if(line.contains("White level"))
                            {
                                String[] data = line.split("=");
                                verificationData.add(data[1].split(" ")[1]);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        thread = new Thread(writeTask);
        thread.start();
    }

    //odpalenie weryfikacji
    public List<String> verification(Monitor monit) {

        String commmand = "cmd.exe /C start /wait cmd.exe /C ^(  data\\dispcal -Y p -v -r -d"+monit.getId()+" -X data\\"+monit.getMonitorType().getCorrectionType()+" ^> ^wynik.txt ^2^>^&^1 ^| echo Trwa weryfikacja^ )";
        boolean exist = false;

        try {
            File yourFile = new File("wynik.txt");
            if(!yourFile.exists()) {
                yourFile.createNewFile();
            }
            PrintWriter writer = new PrintWriter(yourFile);
            writer.close();
            InputStream in = Files.newInputStream(yourFile.toPath());
            startReadVerification(in);
            Process proc = Runtime.getRuntime().exec(commmand);
            proc.waitFor();
            synchronized (lock)
            {
                lock.wait(1000);
            }

            return verificationData;

        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


}
