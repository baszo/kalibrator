package Controller;

import Connection.WebConnection;
import View.IWindows;
import commands.CommandBody;
import commands.CommandsCoder;
import usedObjects.CalibConfiguration;
import usedObjects.Monitor;
import usedObjects.MonitorTypes;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Andrzej on 2016-02-17.
 * Klasa reprezentująca kontrolera do okna zawierająca już skonfigurowane przez użytkownika monitory i
 * możliwość ich kalibracji oraz możliwość edycji
 */
public class MainCalibraionController implements IMainCalibrationController {
    private WebConnection connection;
    CommandsCoder commandsCoder;
    String serverData;
    List<Monitor> monitorList;
    IWindows window;
    DispcalHandler dispcalHandler;

    public MainCalibraionController(WebConnection connection, IWindows window) {
        this.connection = connection;
        this.window = window;
        commandsCoder = new CommandsCoder();
        dispcalHandler = new DispcalHandler();
        monitorList = new ArrayList<>();
        configureMonitors();
    }

    //Pobiera potrzebne dane i opakowuje je na potrzeby ekranu który będzie używał tych danych
    public void configureMonitors() {
        try
        {
            connection.sendMessageToServer(commandsCoder.encodeCommand("GETMONITORS",connection.userName,null));
            serverData = connection.reciveMessageFromServer();
            CommandBody body = commandsCoder.decodeCommand(serverData);
            for(String record: body.getBody())
            {
                if(!record.isEmpty()) {
                    String[] monitor = record.split(Pattern.quote(commandsCoder.innerDelimiter));
                    monitorList.add(new Monitor(Integer.parseInt(monitor[6]),monitor[7],new CalibConfiguration(monitor[0], Integer.parseInt(monitor[1]), Integer.parseInt(monitor[2]),Double.parseDouble(monitor[3])), new MonitorTypes(monitor[4], monitor[5])));
                }
            }
        } catch (SocketTimeoutException e) {
            window.showError(e.getMessage());
        }

    }

    //obsługa żądania od ekranu pobrania monitorów użytkownaika
    @Override
    public List<Monitor> getMonitors() {
        return monitorList;
    }

    //pobranie nazwy użytkownika
    @Override
    public String getUserName() {
        return connection.userName;
    }

    //pobranie używanego w aplikacji połaćzenia do serwera
    @Override
    public WebConnection getConnection() {
        return connection;
    }

    //wysłanie danych z wersyfikacji na serwer
    @Override
    public void sendVerificationData(Monitor mon,List<String> aa) {
        List<String> data = new ArrayList<>();
        String record=mon.getId()+commandsCoder.innerDelimiter;
        for(String field : aa)
        {
            record+=field+commandsCoder.innerDelimiter;
        }
        data.add(record);
        connection.sendMessageToServer(commandsCoder.encodeCommand("ADDVERIFICAION", connection.userName, data));
    }

    //wykonanie podsatawowej kalibracji
    @Override
    public void executeBasicCalibrtion(Monitor mon) {
        dispcalHandler.executeCalibration(mon);
        dispcalHandler.addSchedulerVeryfication(mon,connection.userName,connection);
    }

    //wykonaie zaawanasowanej kalibracji
    @Override
    public void executeAdvanceCalibration(Monitor mon) {
        dispcalHandler.executeAdvanceCalibration(mon);
        dispcalHandler.addSchedulerVeryfication(mon,connection.userName,connection);
    }

    //pobranie przez program dispcal dostępnych monitorów
    @Override
    public List<Monitor> getMoniotrsDetails() {
        return dispcalHandler.getMoniotrsDetails();
    }

    public Monitor getMonitorFromData(int monitorId,String description){
        List<Monitor> monitorLocalDetail = getMoniotrsDetails();
        boolean exist = false;
        Monitor monit = null;

        //sprawdzamy czy monitor który jest w harmonogramie windowsowym jest nadal aktualny
        for(Monitor mon : monitorLocalDetail)
        {
            if(mon.getId() == monitorId && mon.getDescription().equals(description)) {
                exist = true;
                monit = mon;
            }
        }
        return monit;
    }

    //uruchomione z charmonogramu cykliczne weryfikowanie stanu monitora
    @Override
    public boolean verification(int monitorId,String description) {
        //sprawdz czy istnieje monitor z pobranych z serwera dla użytkownika
        Monitor monit = getMonitorFromData(monitorId,description);

        boolean exist = monit!=null;

        //jeżeli istnieje uruchamiana jest weryfikac
        if(!exist)
            window.showError("Ekran  "+monitorId + " zmienił się należy ponownie go skonfigurować");
        else {
            for (Monitor mon: monitorList) {
                if(mon.getId() == monit.getId()) {
                    monit = mon;
                    break;
                }
            }
            List<String> data = dispcalHandler.verification(monit);
            if(!data.isEmpty()) {
                if(!checkIfDataActual(data, monit))
                    exist = false;
                sendVerificationData(monit, data);
            }
        }

        return exist;
    }

    @Override
    public boolean addSchedulerVerificationOneTime(int monitorId, String description) {
        Monitor monit = getMonitorFromData(monitorId,description);
        boolean exist = monit!=null;
        if(!exist)
            window.showError("Ekran  "+monitorId + " zmieniła się należy ponownie go skonfigurować");
        else {
            dispcalHandler.addOneTimeSchedulerVeryfication(monit, connection.userName, connection);
            return true;
        }
        return false;
    }

    //sprawdzamy czy dane aktualne
    private boolean checkIfDataActual(List<String> data, Monitor monit) {
        boolean exist = false;
        for(Monitor mon : monitorList)
        {
            if(mon.getId() == monit.getId() && mon.getDescription().equals(monit.getDescription())) {
                exist = true;
                monit = mon;
            }
        }
        if(exist && !data.isEmpty()) {
            double brigntesChange = Double.parseDouble(data.get(0)) / monit.getConfig().getBrightness();
            double gammaChange = Double.parseDouble(data.get(1)) / monit.getConfig().getGamma();
            double whiteCorrelationChange = Integer.parseInt(data.get(2).trim()) / monit.getConfig().getWhiteCorrolateDaylight();
            if ((brigntesChange > 1.1 || brigntesChange < 0.9) || (gammaChange > 1.1 || gammaChange < 0.9) || (whiteCorrelationChange > 1.1 || whiteCorrelationChange < 0.9)) {
                window.showError("Monitor o identyfikatorze " + monit.getId() + " jest rozkalibrowany");
                dispcalHandler.deleteScheduler(monit.getId());
                exist = false;
            }
        }
        return exist;
    }
}
