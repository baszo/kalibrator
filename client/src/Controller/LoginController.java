package Controller;

import Connection.TcpWebConnection;
import Connection.WebConnection;
import View.IWindows;
import commands.CommandBody;
import commands.CommandsCoder;
import usedObjects.DefaultSetting;

import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrzej on 2016-02-12.
 * Controller do obsługi okna logowania
 */
public class LoginController implements ILoginController, DefaultSetting {
    //gniazdo dla połaczenia z serwerem
    WebConnection connection;
    String serverData;
    CommandsCoder commandsCoder = new CommandsCoder();
    IWindows window;

    public LoginController(IWindows window) {
        this.window =window;
    }


    //loguje się na serwer i tworzy połączenie
    private boolean connectToServer(String serverAddres, int serverPort, List<String> data,String login){
        try
        {
            connection = createConnection(serverAddres,serverPort,login);
            connection.sendMessageToServer(commandsCoder.encodeCommand("Login",data));
            serverData  = connection.reciveMessageFromServer();
            CommandBody cmdBody = commandsCoder.decodeCommand(serverData);
            switch (cmdBody.getCommand())
            {
                case "OK": return true;
                case "NOPE": return false;
            }
        } catch (SocketTimeoutException e) {
            window.showError(e.getMessage());
        }
        return false;
    }

    //wysłanie próby rejestracji na serwer
    private boolean registerToServer(String serverAddres, int serverPort, List<String> data,String login)
    {
        try
        {
            connection = createConnection(serverAddres,serverPort,login);
            connection.sendMessageToServer(commandsCoder.encodeCommand("REGISTER",data));
            serverData  = connection.reciveMessageFromServer();
            CommandBody cmdBody = commandsCoder.decodeCommand(serverData);
            switch (cmdBody.getCommand())
            {
                case "OK": return true;
                case "NOPE": return false;
            }
        } catch (SocketTimeoutException e) {
            window.showError(e.getMessage());
        }
        return false;

    }

    //tworzenie połączenia
    private WebConnection createConnection(String serverAddres, int serverPort,String login)
    {
        return new TcpWebConnection(serverPort,serverAddres,login);
    }


    @Override
    public boolean login(String login, String password, String serverAddres,boolean remamberData) {
        if(remamberData)
            save(login,password,serverAddres);

        return connectToServer(serverAddres,defaultPort,Arrays.asList(login,password),login);

    }

    @Override
    public boolean register(String login, String password, String serverAddres,boolean remamberData) {
        if(remamberData)
            save(login,password,serverAddres);
        return registerToServer(serverAddres,defaultPort,Arrays.asList(login,password),login);
    }

    @Override
    public WebConnection getConnection() {
        return connection;
    }


    public static String getUserDataDirectory() {
        return System.getProperty("user.home") + File.separator + ".calibrator" + File.separator;
    }

    /**
     * Zapisywanie danych do logowania
     *
     * @param login login użytkownika
     * @param password hasło użytkownika
     * @param hostAddres adres servera
     * @return
     */
    private void save(String login, String password, String hostAddres) {
        File file = new File(getUserDataDirectory());
        try {
            if(!file.exists()) file.createNewFile();  //if the file !exist create a new one

            BufferedWriter bw = new BufferedWriter(new FileWriter(file.getAbsolutePath()));
            bw.write(login); //write the name
            bw.newLine(); //leave a new Line
            bw.write(md5Hash(password)); //write the password
            bw.newLine(); //leave a new Line
            bw.write(hostAddres); //write the name
            bw.close(); //close the BufferdWriter

        } catch (IOException e) { e.printStackTrace(); }
    }

    /**
     *
     * Hashowanie hasła dla bezpieczeństwa
     *
     * @param password hasło do zakodowania
     * @return
     */
    public static String md5Hash(String password) {
        String md5 = "";
        if(null == password)
            return null;
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");//Create MessageDigest object for MD5
            digest.update(password.getBytes(), 0, password.length());//Update input string in message digest
            md5 = new BigInteger(1, digest.digest()).toString(16);//Converts message digest value in base 16 (hex)

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5;
    }
}
