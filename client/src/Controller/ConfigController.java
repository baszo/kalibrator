package Controller;

import Connection.WebConnection;
import View.IWindows;
import commands.CommandBody;
import commands.CommandsCoder;
import usedObjects.CalibConfiguration;
import usedObjects.Monitor;
import usedObjects.MonitorTypes;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Andrzej on 2016-02-14.
 * Klasa odpowiedzialna za wykonywanie akcji dla okna do konfiguracji monitorów
 */
public class ConfigController implements IConfigController {
    private WebConnection connection;
    List<MonitorTypes> monitorTypesList;
    List<CalibConfiguration> calibConfigurations;
    String serverData;
    CommandsCoder commandsCoder;
    IWindows window;
    DispcalHandler dispcalHandler;


    //konstruktor
    public ConfigController(WebConnection connection, IWindows window) {
        dispcalHandler = new DispcalHandler();
        this.connection = connection;
        this.window = window;
        commandsCoder = new CommandsCoder();
        monitorTypesList = new ArrayList<>();
        calibConfigurations = new ArrayList<>();
        getServerMonitorTypes();
        getServerCalibratorConfigurations();
    }

    //pobranie rodzajów monitorów z serwera
    private void getServerMonitorTypes() {
        connection.sendMessageToServer(commandsCoder.encodeCommand("GETMONITORTYPES",null));
        try {
            serverData = connection.reciveMessageFromServer();
            CommandBody body = commandsCoder.decodeCommand(serverData);
            body.getBody().stream().filter(record -> !record.isEmpty()).forEach(record -> {
                String[] monitor = record.split(Pattern.quote(commandsCoder.innerDelimiter));
                monitorTypesList.add(new MonitorTypes(monitor[0], monitor[1]));
            });
        } catch (SocketTimeoutException e) {
            window.showError(e.getMessage());
        }
    }

    //pobranie rodzajów konfiguracji z serwera
    private void getServerCalibratorConfigurations() {
        connection.sendMessageToServer(commandsCoder.encodeCommand("GETCONFIGS",null));
        try {
            serverData = connection.reciveMessageFromServer();
            CommandBody body = commandsCoder.decodeCommand(serverData);
            body.getBody().stream().filter(record -> !record.isEmpty()).forEach(record -> {
                String[] config = record.split(Pattern.quote(commandsCoder.innerDelimiter));
                calibConfigurations.add(new CalibConfiguration(config[0], Integer.parseInt(config[1]), Integer.parseInt(config[2]), Double.parseDouble(config[3])));
            });
        } catch (SocketTimeoutException e) {
            window.showError(e.getMessage());
        }
    }


    //przekazanie do widoku rodzajów monitorów
    @Override
    public List<MonitorTypes> getMonitorsConfigurations() {
        return monitorTypesList;
    }

    //przekazanie do widoku rodzajó kalibracji
    @Override
    public List<CalibConfiguration> getCalibConfiguration() {
        return calibConfigurations;
    }

    //przekazanie do widoku opisu monitorów pobranych z dispcal
    public List<Monitor> getMonitorsDetails(){
        return dispcalHandler.getMoniotrsDetails();
    }

    //wysłanie skonfigurowanych monitorów na serwer
    @Override
    public boolean configureMonitors(List<Monitor> monitors) {
        try {
            List<String> data = new ArrayList<>();
            for (Monitor monitor : monitors) {
                CalibConfiguration conf = monitor.getConfig();
                MonitorTypes mon = monitor.getMonitorType();
                data.add(conf.getName() + commandsCoder.innerDelimiter + mon.getModel() + commandsCoder.innerDelimiter + monitor.getId() +commandsCoder.innerDelimiter + monitor.getDescription());
            }
            connection.sendMessageToServer(commandsCoder.encodeCommand("ADDMONITORS", connection.userName, data));
            switch (commandsCoder.decodeCommand(connection.reciveMessageFromServer()).getCommand()) {
                case "SUCCESS":
                    return true;
            }
        } catch (SocketTimeoutException e) {
            window.showError(e.getMessage());
        }
        return false;

    }

    //przekazanie połączenia do kolejnego okna
    @Override
    public WebConnection getConnection() {
        return connection;
    }


}


