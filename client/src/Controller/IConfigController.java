package Controller;


import Connection.WebConnection;
import usedObjects.CalibConfiguration;
import usedObjects.Monitor;
import usedObjects.MonitorTypes;

import java.util.List;

/**
 * Created by Andrzej on 2016-02-14.
 * interfejs dla okna Konfiguracji monitorów do komunikacji z controlerem
 */
public interface IConfigController {
    List<MonitorTypes> getMonitorsConfigurations();
    List<CalibConfiguration> getCalibConfiguration();
    List<Monitor> getMonitorsDetails();
    boolean configureMonitors(List<Monitor> monitors);

    WebConnection getConnection();
}
