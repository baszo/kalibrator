package Controller;


import Connection.WebConnection;
import usedObjects.Monitor;

import java.util.List;

/**
 * Created by Andrzej on 2016-02-17.
 * interfejs służący do informowania controlera o potrzebnych zasobach lub akcjach które trzeba wykonać
 */
public interface IMainCalibrationController {
    List<Monitor> getMonitors();

    String getUserName();

    WebConnection getConnection();

    void sendVerificationData(Monitor mon,List<String> data);

    void executeBasicCalibrtion(Monitor mon);

    void executeAdvanceCalibration(Monitor mon);

    List<Monitor> getMoniotrsDetails();

    boolean verification(int monitorId,String description);

    boolean addSchedulerVerificationOneTime(int monitorId, String description);
}
