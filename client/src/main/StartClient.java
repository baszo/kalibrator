package main;

import View.Login;
import View.MainCalibrationPage;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Andrzej on 2016-02-12.
 * Klasa startująca program kliencki
 */


public class StartClient {
    public static void main(String [ ] args) {

        //jeżeli jest to weryfikacja z schtask
        if(args.length!=0)
        {
            int monitorId = Integer.parseInt(args[0]);
            String description = " '"+args[1]+"'";
            String user = args[2];
            String serverAddres = args[3];
            new MainCalibrationPage(monitorId,description,user,serverAddres);
        }
        //jak nie zwykły start aplikacji
        else {
            new Login();
        }
    }

}
