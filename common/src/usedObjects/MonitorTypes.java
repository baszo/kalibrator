package usedObjects;

/**
 * Created by Andrzej on 2016-02-14.
 * Klasa reprezuntująca model monitora
 */
public class MonitorTypes {
    private String model;
    private String correctionType;


    public MonitorTypes(String model) {
        this.model = model;
    }
    public MonitorTypes(String model, String correctionType) {
        this.model = model;
        this.correctionType = correctionType;
    }


    public String getModel() {
        return model;
    }

    public String getCorrectionType() {
        return correctionType;
    }
}
