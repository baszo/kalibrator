package usedObjects;

/**
 * Created by Andrzej on 2016-02-14.
 * Klasa przechowująca dane o typie konfiguracji monitora
 */
public class CalibConfiguration {
    private String name;
    private int brightness;
    private int whiteCorrolateDaylight;
    private double gamma;



    public CalibConfiguration(String name, int brightness, int whiteCorrolateDaylight,double gamma) {
        this.name = name;
        this.brightness = brightness;
        this.whiteCorrolateDaylight = whiteCorrolateDaylight;
        this.gamma = gamma;
    }

    public String getName() {
        return name;
    }

    public int getBrightness() {
        return brightness;
    }

    public int getWhiteCorrolateDaylight() {
        return whiteCorrolateDaylight;
    }

    public double getGamma() {
        return gamma;
    }
}
