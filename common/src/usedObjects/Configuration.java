package usedObjects; /**
 * Created by andrzej2 on 09.11.15.
 * Klasa reprezuntująca singleton do obsługi propertisów
 */

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

//klasa odpowiadająca za ładowanie konfiguracji
public class Configuration {
    private static Configuration _instance = null;

    private Properties props = null;

    private Configuration() {
        props = new Properties();
        try {
            FileInputStream fis = new FileInputStream(new File("src/config.properties"));
            props.load(fis);
        }
        catch (Exception e) {
            // catch resources.usedObjects.Configuration Exception right here
        }
    }

    public synchronized static Configuration getInstance() {
        if (_instance == null)
            _instance = new Configuration();
        return _instance;
    }

    // get property value by name
    public String getProperty(String key) {
        String value = null;
        if (props.containsKey(key))
            value = (String) props.get(key);
        else {
            // property brak
        }
        return value;
    }
    public String getPropertyWithDefault(String key,String defaultValue) {
        String value = null;
        if (props.containsKey(key))
            value = (String) props.get(key);
        else {
            return defaultValue;
        }
        return value;
    }

    // get property value by name
    public String setProperty(String key,String value) {
        if (props.containsKey(key))
            value = (String) props.setProperty(key, value);
        else {
            // property brak
        }
        return value;
    }
}