package usedObjects;

/**
 * Created by Andrzej on 2016-02-17.
 * Klasa reprezuntująca monitor
 */
public class Monitor {
    int id;
    String description;
    CalibConfiguration config;
    MonitorTypes monitorType;


    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setConfig(CalibConfiguration config) {
        this.config = config;
    }

    public void setMonitorType(MonitorTypes monitorType) {
        this.monitorType = monitorType;
    }

    public Monitor(int id,String description) {
        this.description = description;
        this.id = id;
    }



    public Monitor(int id,String description,CalibConfiguration config, MonitorTypes monitorType) {
        this.id = id;
        this.description=description;
        this.config = config;
        this.monitorType = monitorType;
    }

    public CalibConfiguration getConfig() {
        return config;
    }

    public MonitorTypes getMonitorType() {
        return monitorType;
    }
}
