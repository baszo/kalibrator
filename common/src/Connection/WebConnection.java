package Connection;

import java.net.SocketTimeoutException;

/**
 * Created by Andrzej on 2016-02-17.
 */
public abstract class WebConnection {
    public abstract void sendMessageToServer(String message);
    public abstract String reciveMessageFromServer() throws SocketTimeoutException;
    public String userName;
    public abstract String getServerAddres();
}
