package Connection;

import java.io.IOException;
import java.net.*;

/**
 * Created by Andrzej on 2016-02-17.
 * Klasa reprezentująca kounikacjae za pomocą pakietów TCP
 */
public class TcpWebConnection extends WebConnection {
    DatagramSocket socket;
    byte[] packet = new byte[1024];
    DatagramPacket datagram;
    int serverPort;
    String serverAddres;

    public TcpWebConnection(int serverPort, String serverAddres) {
        try {
            this.serverPort = serverPort;
            this.serverAddres = serverAddres;
            socket = new DatagramSocket();
            socket.setSoTimeout(10000);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }
    public TcpWebConnection(int serverPort, String serverAddres,String user) {
        try {
            this.serverPort = serverPort;
            this.serverAddres = serverAddres;
            socket = new DatagramSocket();
            socket.setSoTimeout(10000);
            this.userName = user;
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendMessageToServer(String Message)
    {
        try {
            socket.send(new DatagramPacket(Message.getBytes(), 0, Message.length(), InetAddress.getByName(serverAddres), serverPort));
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public String reciveMessageFromServer() throws SocketTimeoutException {
        String command = "";
        try {
            datagram = new DatagramPacket(packet, packet.length);
            socket.receive(datagram);
            command = new String(datagram.getData(), 0, datagram.getLength());
        }
        catch (SocketTimeoutException e) {
            // resend
            throw e;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return command;
    }

    @Override
    public String getServerAddres() {
        return serverAddres;
    }
}
