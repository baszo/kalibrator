package commands;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrzej on 2016-02-12.
 */
public class Tests {


    public static void main(String [ ] args)
    {
        String commandencode = "List~ja;arg1;arg2;arg3";
        String wrong = "~;";
        String command = "List";
        String from = "Andrzej";
        List<String> body = Arrays.asList("asdasd-asdasd-asdas", "asdasdasd~asdasd", "sup3");
        CommandsCoder coder = new CommandsCoder();

        System.out.println(coder.encodeCommand(command,body));
        System.out.println(coder.decodeCommand(commandencode).getCommand());
        System.out.println(coder.decodeCommand(wrong).getBody());
        System.out.println(coder.decodeCommand("").getBody());

    }


}
