package commands;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrzej on 2016-02-12.
 * Klasa odpowiedzialna za kodowanie oraz dekodowanie wiadomości między
 * klientem i serwerem.
 * Zawiera znaki rodzielające rekordy,elementy i nagłówki
 */
public class CommandsCoder {
    public String recordSeparator = ";";
    public String headerSeparator = "~";
    public String innerDelimiter  = "|";

    //odczytanie wiadomoścu
    public CommandBody decodeCommand(String commandString)
    {
        try {
            String command = commandString.substring(0, commandString.indexOf(headerSeparator));
            List<String> body = Arrays.asList(commandString.substring(commandString.indexOf(headerSeparator) + 1).split(recordSeparator));

            return new CommandBody(command,body);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    //odczyanie wiadomości
    public CommandBody decodeCommandWithUser(String commandString)
    {
        try {
            String[] splitCommand = commandString.split(headerSeparator);
            String command = splitCommand[0];
            String user = splitCommand[1];
            List<String> body = Arrays.asList(splitCommand[2].split(recordSeparator));

            return new CommandBody(command,user,body);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    //generacja wiadmości
    public String encodeCommand(String command, List<String> arg)
    {
        if(arg == null)
            return command + headerSeparator + recordSeparator;
        return command + headerSeparator + String.join(recordSeparator,arg);
    }

    //generacja wiadomości
    public String encodeCommand(String command, String user, List<String> arg)
    {
        if(arg == null || arg.isEmpty())
            return command + headerSeparator + user + headerSeparator + recordSeparator;
        return command + headerSeparator + user + headerSeparator + String.join(recordSeparator,arg);
    }
}
