package commands;

import java.net.DatagramPacket;
import java.net.Socket;
import java.util.List;

/**
 * Created by Andrzej on 2016-02-12.
 * Klasas reprezentująca komendy używane do komunikacji klient serwer
 */
public abstract class Command {
    private String name;

    protected Command(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void execute(DatagramPacket packet, String command);
}
