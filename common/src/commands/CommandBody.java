package commands;

import java.util.List;
import java.util.StringJoiner;

/**
 * Created by Andrzej on 2016-02-12.
 * Ciało komendy do komunikacji klient-serweer
 */
public class CommandBody {
    private String command;
    private List<String> body;

    public String getUser() {
        return user;
    }

    private String user;

    public CommandBody(String command, List<String> body) {
        this.command = command;
        this.body = body;
    }
    public CommandBody(String command, String user, List<String> body) {
        this.command = command;
        this.user = user;
        this.body = body;
    }

    public String getCommand() {
        return command;
    }

    public List<String> getBody() {
        return body;
    }
}
